package edu.leipzig.BigdataPraktikum.model;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.Random;

public class Company implements Element {
    private static Faker faker;
    private String Id;
    private ArrayList<Project> projects;
    private String city;
    private String name;

    Company(String Id, String name, String city) {
        this.name = name;
        this.Id = Id;
        this.projects = new ArrayList<>();
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return Id;
    }

    ArrayList<Project> getProjects() {
        return projects;
    }

    void addProject(Project p) {
        this.projects.add(p);
    }

    Project getRandomProject() {
        if (this.projects.size() > 0) {
            Random rand = new Random();
            int projectIndex = rand.nextInt(this.projects.size());
            return this.projects.get(projectIndex);
        } else return null;
    }

    @Override
    public String toString() {
        int version = faker.number().numberBetween(0, 10);
        return String.format("{\"id\":\"%s\",\"label\":\"%s\"," +
                        "\"properties\":{\"name\":\"%s\", \"city\":\"%s\"}}",
                Id, this.getClass().getSimpleName() + version, name, city);
    }

    @Override
    public String getIdWithLabel() {
        return "Company:" + this.Id;
    }
}
