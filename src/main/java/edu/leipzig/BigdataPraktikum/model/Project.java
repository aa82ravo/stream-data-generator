package edu.leipzig.BigdataPraktikum.model;

import com.github.javafaker.Faker;

public class Project implements Element{
    private static Faker faker;
    static int count = 0; // static count all projects in run
    private String name;
    private String Id;
    private String domain;

    public String getDomain() {
        return domain;
    }

    public String getName() {
        return name;
    }

    Project(String Id, String name, String domain) {
        this.name = name;
        this.Id = Id;
        this.domain = domain;
    }

    @Override
    public String toString() {
        int version = faker.number().numberBetween(0, 10);
        return String.format("{\"id\":\"%s\",\"label\":\"%s\"," +
                        "\"properties\":{\"name\":\"%s\",\"domain\":\"%s\"}}",
                Id, this.getClass().getSimpleName() + version, name, domain);
    }

    @Override
    public String getId() {
        return this.Id;
    }

    @Override
    public String getIdWithLabel() {
        return "Project:" + this.Id;
    }
}
