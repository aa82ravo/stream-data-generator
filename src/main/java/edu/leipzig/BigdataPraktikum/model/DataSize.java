package edu.leipzig.BigdataPraktikum.model;

public enum DataSize {
    TINY,
    SMALL,
    MEDIUM,
    LARGE,
    HUGE
}
