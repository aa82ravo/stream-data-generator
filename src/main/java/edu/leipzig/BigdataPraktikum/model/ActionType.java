package edu.leipzig.BigdataPraktikum.model;

public enum ActionType {
    COMMIT,
    MEMBER_OF,
    BELONGS_TO,
    KNOWS
}
