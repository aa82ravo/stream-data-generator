package edu.leipzig.BigdataPraktikum.model;

public interface Element {
    String getIdWithLabel();
    String getId();
}
