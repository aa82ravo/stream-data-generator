package edu.leipzig.BigdataPraktikum.model;

import java.sql.Timestamp;

public class Action<S extends Element, T extends Element> {
    private Long timestamp;
    private ActionType label;
    private String Id;
    private S source;
    private T target;
    private String month;

    Action(S source, T target, ActionType type, String Id, String month) {
        this.source = source;
        this.target = target;
        this.label = type;
        this.Id = Id;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        this.timestamp = timestamp.getTime();
        this.month = month;
    }

    @Override
    public String toString() {
        return String.format("{\"id\":\"%s\",\"label\":\"%s\"," +
                        "\"properties\":{\"month\":\"%s\"}," +
                        "\"source\":%s, \"target\":%s, \"timestamp\":\"%d\"}",
                Id, label, month, source.toString(), target.toString(), timestamp);
    }

    public String toCsvString() {
        return String.format("%s, %s, %s, %s, %s, %s",
                Id, label, month, source.getIdWithLabel(), target.getIdWithLabel(), timestamp);
    }
}
