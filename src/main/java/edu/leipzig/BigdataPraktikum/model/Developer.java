package edu.leipzig.BigdataPraktikum.model;

import com.github.javafaker.Faker;

public class Developer implements Element {
    private static Faker faker;
    private String name;
    private String Id;
    String companyId;

    public String getName() {
        return name;
    }

    public long getSalary() {
        return salary;
    }

    private long salary;

    Developer(String Id, String name, String companyId, long salary) {
        this.name = name;
        this.Id = Id;
        this.companyId = companyId;
        this.salary = salary;
    }

    @Override
    public String toString() {
        int version = faker.number().numberBetween(0, 10);
        return String.format("{\"id\":\"%s\",\"label\":\"%s\"," +
                        "\"properties\":{\"name\":\"%s\", \"salary\":%d}}",
                Id, this.getClass().getSimpleName() + version, name, salary);
    }

    @Override
    public String getId() {
        return this.Id;
    }

    @Override
    public String getIdWithLabel() {
        return "Developer:" + this.Id;
    }
}
