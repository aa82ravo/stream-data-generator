package edu.leipzig.BigdataPraktikum.model;

import com.github.javafaker.Faker;
import edu.leipzig.BigdataPraktikum.Generator;
import edu.leipzig.BigdataPraktikum.network.SocketServer;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class FakeDataGenerator {
    private static Random rand;
    private static ArrayList<Company> companies;
    private static ArrayList<Developer> developers;
    private static int devCount;
    private static int companyCount;
    private static int projectCount;
    private static ArrayList<String> cities;
    private static ArrayList<String> Months = new ArrayList<>
            (Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"));
    private static ArrayList<String> projectDomains;
    private static Faker faker;

    private static void init(DataSize size, boolean toBeLogged) {
        rand = new Random();
        companies = new ArrayList<>();
        developers = new ArrayList<>();
        cities = new ArrayList<>();
        projectDomains = new ArrayList<>();
        faker = new Faker();
        devCount = 0;
        switch (size) {
            case TINY:
                devCount = faker.number().numberBetween(1, 6);
                break;
            case SMALL:
                devCount = faker.number().numberBetween(10, 30);
                break;
            case MEDIUM:
                devCount = faker.number().numberBetween(31, 60);
                break;
            case LARGE:
                devCount = faker.number().numberBetween(61, 120);
                break;
            case HUGE:
                devCount = faker.number().numberBetween(500, 1000);
                break;
        }
        companyCount = devCount / 3;
        companyCount = (companyCount == 0 ? 1 : companyCount);
        projectCount = faker.number().numberBetween(companyCount, devCount - 1);
        if (toBeLogged) {
            writeStats(devCount, companyCount, projectCount);
        }
    }

    private static void writeStats(int devCount, int companyCount, int projectCount) {

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("stats.txt", false);
            DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));
            outStream.writeUTF(String.format("DevCount: %d\nCompanyCount: %d\nProjectCount: %d\nMessagesCount: %d",
                    devCount, companyCount, projectCount, SocketServer.count));
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeInsights(String projectsInsight) {

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("insights.txt", false);
            DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));
            outStream.writeUTF("first 10 Companies: -------------------\n");
            int cnt = Math.min(companies.size(), 10);
            for (int i = 0; i < cnt; i++) {
                outStream.writeUTF(String.format("{\"name\":\"%s\", \"city\":\"%s\"}}\n",
                        companies.get(i).getName(), companies.get(i).getCity()));
            }
            outStream.writeUTF("first 10 Developers: -------------------\n");
            cnt = Math.min(developers.size(), 10);
            for (int i = 0; i < cnt; i++) {
                outStream.writeUTF(String.format("{\"name\":\"%s\", \"salary\":\"%s\"}}\n",
                        developers.get(i).getName(), developers.get(i).getSalary()));
            }
            outStream.writeUTF("first 10 Cities: -------------------\n");
            cnt = Math.min(cities.size(), 10);
            for (int i = 0; i < cnt; i++) {
                outStream.writeUTF(cities.get(i) + ", \n");
            }
            if (!projectsInsight.equals("")) {
                outStream.writeUTF("first 10 Projects: -------------------\n");
                outStream.writeUTF(projectsInsight);
            }
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean generate(DataSize size, int interval, SocketServer connection, boolean useHistoricalData, boolean toBeLogged, int multiplyHistorical)
            throws InterruptedException, IOException {
        init(size, toBeLogged);
        if (useHistoricalData) {
            if (multiplyHistorical <= 0) multiplyHistorical = 1;
            for (int i = 0; i < multiplyHistorical; i++) {
                readOldBaseSchema(connection, interval);
            }
        }
        generateBaseSchema(toBeLogged);
        sendBaseSchema(connection, useHistoricalData, toBeLogged, interval);
        /*
         * continuous sending new generated data as long as the client connected
         * */
        System.out.println("Sending started");
        while (!connection.checkConnection()) { // while the connection is up
            if (Generator.checkControlData()) {
                connection.sendControlMsg(Generator.getControlData());
                Generator.resetControlData();
            }
            generateNewCommit(connection, toBeLogged);
            generateNewEntities(connection, toBeLogged);
            // sleep
            // int sleepInSec = faker.number().numberBetween(1, 10);
            // System.out.println("Thread will sleep for: " + sleepInSec);
            Thread.sleep(1,interval);
        }
        System.out.println("Sending terminated");
        return true;
    }

    private static void generateNewEntities(SocketServer connection, boolean toBeLogged) throws IOException {
        int generateProb = faker.number().numberBetween(0, 19);
        if (generateProb < 4) { // low prop range generate Company
            companies.add(new Company(faker.idNumber().valid(), faker.company().name(), cities.get(faker.number().numberBetween(0, cities.size() - 1))));
        } else if (generateProb < 10) { // mid prop range generate project
            int companyIndex = faker.number().numberBetween(0, companies.size() - 1);
            Project tempProject = new Project(faker.idNumber().valid(), faker.app().name(),
                    projectDomains.get(faker.number().numberBetween(0, projectDomains.size() - 1)));
            companies.get(companyIndex).addProject(tempProject);
            Project.count++;
            Action<Project, Company> tempAction = new Action<>(tempProject, companies.get(companyIndex), ActionType.BELONGS_TO,
                    faker.idNumber().valid(), Months.get(faker.number().numberBetween(0, Months.size() - 1)));
            connection.send(tempAction.toString(), toBeLogged, tempAction.toCsvString());
        } else if (generateProb < 19) { // high prop range generate dev
            int companyIndex = faker.number().numberBetween(0, companies.size() - 1);
            Developer tempDev = new Developer(faker.idNumber().valid(), faker.name().name(), companies.get(companyIndex).getId(),
                    faker.number().numberBetween(2000L, 5000L));
            developers.add(tempDev);
            Action<Developer, Company> tempAction = new Action<>(tempDev, companies.get(companyIndex), ActionType.MEMBER_OF, faker.idNumber().valid(),
                    Months.get(faker.number().numberBetween(0, Months.size() - 1)));
            connection.send(tempAction.toString(), toBeLogged, tempAction.toCsvString());
        }
        if (toBeLogged) writeStats(developers.size(), companies.size(), Project.count);
    }

    private static void generateNewCommit(SocketServer connection, boolean toBeLogged) throws IOException {
        Developer developer = getRandomDeveloper(developers);
        Company devComp = companies.stream()
                .filter(company -> developer.companyId.equals(company.getId()))
                .findAny()
                .orElse(null);
        // send a COMMIT action for random Dev => Project combination
        if ((devComp != null ? devComp.getRandomProject() : null) != null) {
            // System.out.println(new Action<Developer, Project>(developer, devComp.getRandomProject(), ActionType.COMMIT, faker.idNumber().valid()));
            Action<Developer, Project> tempAction = new Action<>(developer, devComp.getRandomProject(), ActionType.COMMIT, faker.idNumber().valid(),
                    Months.get(faker.number().numberBetween(0, Months.size() - 1)));
            connection.send(tempAction.toString(), toBeLogged, tempAction.toCsvString());
        }
        Developer developer1 = getRandomDeveloper(developers);
        Developer developer2 = getRandomDeveloper(developers);
        int generateProb = faker.number().numberBetween(0, 5);
        //we want loops
        if (developer1.equals(developer2) && generateProb < 3) {
            Action<Developer, Developer> tempAction = new Action<>(developer1, developer2, ActionType.KNOWS, faker.idNumber().valid(),
                    Months.get(faker.number().numberBetween(0, Months.size() - 1)));
            //connection.send(tempAction.toString(), toBeLogged, tempAction.toCsvString());
        }
    }

    private static void generateBaseSchema(boolean toBelogged) {
        // fill the available cities
        generateBaseCities();
        // fill the available companies
        generateBaseCompanies();
        // fill the available project-domains
        generateBaseProjectDomain();
        // fill the available Projects
        String projectsInsight = generateBaseProjects();
        // fill the available devs
        generateBaseDevelopers();
        if (toBelogged) writeInsights(projectsInsight);
    }

    private static void generateBaseDevelopers() {
        for (int i = 0; i < devCount; i++) {
            int companyIndex = rand.nextInt(companyCount);
            developers.add(new Developer(faker.idNumber().valid(), faker.name().name(), companies.get(companyIndex).getId(),
                    faker.number().numberBetween(2000L, 5000L)));
        }
    }

    private static String generateBaseProjects() {
        Project.count = projectCount;
        StringBuilder insight = new StringBuilder();
        for (int i = 0; i < projectCount; i++) {
            int companyIndex = rand.nextInt(companyCount);
            Project tmpProject = new Project(faker.idNumber().valid(), faker.app().name(),
                    projectDomains.get(faker.number().numberBetween(0, projectDomains.size() - 1)));
            companies.get(companyIndex).addProject(tmpProject);
            if (i <= 10) insight.append(tmpProject.toString()).append(", ");
        }
        return insight.toString();
    }

    private static void generateBaseCompanies() {
        for (int i = 0; i < companyCount; i++) {
            companies.add(new Company(faker.idNumber().valid(), faker.company().name(),
                    cities.get(faker.number().numberBetween(0, cities.size() - 1))));
        }
    }

    private static void generateBaseCities() {
        for (int i = 0; i < (companyCount / 2) + 1; i++) {
            cities.add(faker.address().cityName());
        }
    }

    private static void generateBaseProjectDomain() {
        for (int i = 0; i < (projectCount / 2) + 1; i++) {
            projectDomains.add(faker.company().industry());
        }
    }

    private static void sendBaseSchema(SocketServer connection, boolean historical, boolean toBeLogged, int interval) throws IOException, InterruptedException {
        /*if (historical) {
            readOldBaseSchema(connection, interval);
        }*/

        developers.forEach((developer) -> {
            Company devComp = companies.stream()
                    .filter(company -> developer.companyId.equals(company.getId()))
                    .findAny()
                    .orElse(null);
            Action<Developer, Company> a = new Action<>(developer, devComp, ActionType.MEMBER_OF, faker.idNumber().valid(),
                    Months.get(faker.number().numberBetween(0, Months.size() - 1)));

            try {
                connection.send(a.toString(), toBeLogged, a.toCsvString());
                Thread.sleep(interval);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        companies.forEach(company -> {
            company.getProjects().forEach(project -> {
                Action<Project, Company> a = new Action<Project, Company>(project, company, ActionType.BELONGS_TO, faker.idNumber().valid(),
                        Months.get(faker.number().numberBetween(0, Months.size() - 1)));
                try {
                    connection.send(a.toString(), toBeLogged, a.toCsvString());
                    Thread.sleep(interval);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            });
        });
    }

    private static void readOldBaseSchema(SocketServer connection, long interval) throws IOException, InterruptedException {
        FileInputStream fis = new FileInputStream("data\\myfile.txt");
        DataInputStream reader = new DataInputStream(fis);
        String line = null;
        while (reader.available() > 0) {
            line = reader.readUTF();
            // line = line.substring(0, line.indexOf("\r\n"));
            connection.send(line, false, "");
            Thread.sleep(interval);
        }
        reader.close();
    }

    private static Developer getRandomDeveloper(ArrayList<Developer> developers) {
        Random rand = new Random();
        int Index = rand.nextInt(developers.size());
        return developers.get(Index);
    }

}
