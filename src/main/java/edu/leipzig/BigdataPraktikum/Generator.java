package edu.leipzig.BigdataPraktikum;

import edu.leipzig.BigdataPraktikum.model.DataSize;
import edu.leipzig.BigdataPraktikum.network.SocketServer;
import io.undertow.Undertow;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;

import java.util.ArrayList;
import java.util.Arrays;

import static io.undertow.Handlers.*;


/**
 * Main entry to the generator, it initialises the socket server which will send the generated data to the
 * connected Flink socket source. And the control signal web socket server, which receives control messages from
 * a connected frontend (grouping, windowing scheme) to be passed along the stream to manipulate Flink stream as Flink
 * does not offer a proper way to restart the stream job yet and we don't want to arbitrary kill the running process
 */
public class Generator {
    public static String restartData = ""; // the information extracted from the control signal
    private static String controlSocketListenPath = "/control";
    private static ArrayList<WebSocketChannel> channels = new ArrayList<>(); //the connected web channels
    private static int controlSocketPort = 8081;
    private static int dataSocketPort = 6661;
    private static DataSize dataSize = DataSize.LARGE;
    private static int interval = 10; // millisecond
    private static boolean toBeLogged = false;
    private static boolean useHistoricalData = false;
    private static int multiplyHistorical = 1;
    private static SocketServer controlServer;

    public static String getControlData() {
        return restartData;
    }

    public static void resetControlData() {
        Generator.restartData = "";
    }

    public static void main(String[] args) throws Exception {
        try {
            dataSocketPort = Integer.parseInt(args[0]);
            controlSocketPort = Integer.parseInt(args[1]);
            dataSize = DataSize.valueOf(args[2]);
            interval = Integer.parseInt(args[3]);
            toBeLogged = Boolean.parseBoolean(args[4]);
            multiplyHistorical = Integer.parseInt(args[5]);
            System.out.println(String.format("Running using params:\n data port: %d, control port: %d, data size: %s, interval: %d millisecond, logging: %b, multiplyHistorical: %d",
                    dataSocketPort, controlSocketPort, dataSize.toString(), interval, toBeLogged, multiplyHistorical));
            // initWebSocketServer();
            initSocketServer();
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            System.out.println(e);
            controlServer.stopServer();
            System.out.println(String.format("DATA INPUT PROBLEM! or no input \nRunning using DEFAULT params data\n port: %d, control port: %d, data size: %s, interval: %d millisecond, logging: %b, multiplyHistorical: %d\"",
                    dataSocketPort, controlSocketPort, dataSize.toString(), interval, toBeLogged, multiplyHistorical));
            // initWebSocketServer();
            initSocketServer();
        }
    }

    private static void initSocketServer() throws Exception {
         controlServer = new SocketServer();
        controlServer.start(dataSocketPort, interval, dataSize, toBeLogged, useHistoricalData, multiplyHistorical);
    }

    private static void initWebSocketServer() {
        Undertow webSendServer = Undertow.builder().addHttpListener(controlSocketPort, "0.0.0.0")
                .setHandler(path().addPrefixPath(controlSocketListenPath, websocket((exchange, channel) -> {
                    channels.add(channel);
                    channel.getReceiveSetter().set(getListener());
                    channel.resumeReceives();
                })).addPrefixPath("/", resource(new ClassPathResourceManager(Generator.class.getClassLoader(),
                        Generator.class.getPackage()))/*.addWelcomeFiles("index.html").setDirectoryListingEnabled(true)*/))
                .build();
        webSendServer.start();
    }

    private static AbstractReceiveListener getListener() {
        return new AbstractReceiveListener() {
            @Override
            protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) {
                final String messageData = message.getData();
                restartData = messageData;
                if (messageData.startsWith("\"restart!")) {
                    restartData = messageData;
                }
                for (WebSocketChannel session : channel.getPeerConnections()) {
                    System.out.println(messageData);
                    WebSockets.sendText(messageData + "122121", session, null);
                }
            }
        };
    }

    public static boolean checkControlData() {
        return !restartData.equals("");
    }
}
