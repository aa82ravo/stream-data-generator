package edu.leipzig.BigdataPraktikum.network;

import edu.leipzig.BigdataPraktikum.model.DataSize;
import edu.leipzig.BigdataPraktikum.model.FakeDataGenerator;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer {
    public static int count = 0;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private FileWriter fw;
    private PrintWriter fileout;
    private BufferedWriter bw;


    /*
     * start and wait until a client is connected, then start to send the data
     * if the connection drops wait until new is established.
     * */
    public void start(int port, int interval, DataSize dataSize, boolean toBeLogged, boolean useHistoricalData, int multiplyHistorical) throws Exception {

        serverSocket = new ServerSocket(port);
        while (true) {
            if (toBeLogged){
                File file =new File("data/myfile.txt");
                if(!file.exists()){
                    file.createNewFile();
                }
                fw = new FileWriter(file, true);
                bw = new BufferedWriter(fw);
                fileout = new PrintWriter(bw);
            }
            System.out.println("Waiting for the client request");
            clientSocket = serverSocket.accept();
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            /*
             * the flowing function loops as long as the connection is up, otherwise it will return to allow the
             * connection to be stopped and wait for another one.
             * */
            FakeDataGenerator.generate(dataSize, interval, this, useHistoricalData, toBeLogged, multiplyHistorical);
            this.stopConnection();
            bw.close();
            Thread.sleep(1000);
        }
    }

    private void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    public void stopServer() throws IOException {
        serverSocket.close();
    }

    public void send(String message, boolean toBeLogged, String logMessage) throws IOException {
        if (toBeLogged) {
            fileout.println(message);
            /*if (!logMessage.equals("")) {
                FileOutputStream csvfos = new FileOutputStream("csv.txt", true);
                DataOutputStream csvoutStream = new DataOutputStream(new BufferedOutputStream(csvfos));
                csvoutStream.writeUTF(logMessage+"\n");
                csvoutStream.close();
            }*/
        }
        out.println(message);
        SocketServer.count++;
        // System.out.println(message);
    }


    public void sendControlMsg(String msg) throws IOException {
        this.send(msg, false, "");
    }

    public boolean checkConnection() {
        return out.checkError();
    }

}
